#include "Original_Blendshapes.h"

using namespace std;
using namespace vvr;

//******************** global vertices *********************

float w[10] = { 0.1 }, w_l[10] = { 0.1 }, w_d[10] = { 0.1 };
int blend_choice = 0;
const string face_names[10] = { "face-reference","anger","cry","fury","grin","laugh","rage","sad","smile","surprise" };
bool linear = true, differential = false, hide = false;

//**********************************************************

Blend_Shapes::Blend_Shapes()
{
	//! Load settings.
	vvr::Shape::DEF_LINE_WIDTH = 4;
	vvr::Shape::DEF_POINT_SIZE = 10;
	m_perspective_proj = true;
	m_bg_col = Colour("768E77");
	m_obj_col = Colour("454545");
	const string objDir = getBasePath() + "face-poses/";
	cout << " please wait to load the blend shapes " << endl;
	// -----------> load original expressions <--------------
	string objFile = objDir + "face-reference.obj";
	original_expressions[0] = vvr::Mesh(objFile);
	blend_1[0] = original_expressions[0];
	objFile = objDir + "face-01-anger.obj";
	original_expressions[1] = vvr::Mesh(objFile);
	blend_1[1] = original_expressions[1];
	objFile = objDir + "face-02-cry.obj";
	original_expressions[2] = vvr::Mesh(objFile);
	blend_1[2] = original_expressions[2];
	objFile = objDir + "face-03-fury.obj";
	original_expressions[3] = vvr::Mesh(objFile);
	blend_1[3] = original_expressions[3];
	objFile = objDir + "face-04-grin.obj";
	original_expressions[4] = vvr::Mesh(objFile);
	blend_1[4] = original_expressions[4];
	objFile = objDir + "face-05-laugh.obj";
	original_expressions[5] = vvr::Mesh(objFile);
	blend_1[5] = original_expressions[5];
	objFile = objDir + "face-06-rage.obj";
	original_expressions[6] = vvr::Mesh(objFile);
	blend_1[6] = original_expressions[6];
	objFile = objDir + "face-07-sad.obj";
	original_expressions[7] = vvr::Mesh(objFile);
	blend_1[7] = original_expressions[7];
	objFile = objDir + "face-08-smile.obj";
	original_expressions[8] = vvr::Mesh(objFile);
	blend_1[8] = original_expressions[8];
	objFile = objDir + "face-09-surprise.obj";
	original_expressions[9] = vvr::Mesh(objFile);
	blend_1[9] = original_expressions[9];
	// -------------------------------------------------------------
	cout << " loading the blend shapes has almost finished " << endl;
	// ----------> initial blend shape    <----------------
	blend_choice_1 = original_expressions[0];

	change_location(original_expressions[0].getVertices(), blend_1[0].getVertices(), 0.0, 2.5);
	change_location(original_expressions[1].getVertices(), blend_1[1].getVertices(), 1.25, 2.02);
	change_location(original_expressions[2].getVertices(), blend_1[2].getVertices(), 2.38, 0.77);
	change_location(original_expressions[3].getVertices(), blend_1[3].getVertices(), 2.38, -0.77);
	change_location(original_expressions[4].getVertices(), blend_1[4].getVertices(), 1.25, -2.02);
	change_location(original_expressions[5].getVertices(), blend_1[5].getVertices(), 0.0, -2.5);
	change_location(original_expressions[6].getVertices(), blend_1[6].getVertices(), -1.25, -2.02);
	change_location(original_expressions[7].getVertices(), blend_1[7].getVertices(), -2.38, -0.77);
	change_location(original_expressions[8].getVertices(), blend_1[8].getVertices(), -2.38, 0.77);
	change_location(original_expressions[9].getVertices(), blend_1[9].getVertices(), -1.25, 2.02);
	cout << " loading the blendshapes is finished " << endl;
	reset();

}

void Blend_Shapes::reset()
{
	Scene::reset();
	//! Define what will be vissible by default
	m_style_flag = 0;
	m_style_flag |= FLAG_SHOW_SOLID;
	m_style_flag |= FLAG_SHOW_WIRE;
	m_style_flag |= FLAG_SHOW_AXES;
	m_style_flag |= FLAG_SHOW_AABB;
}

void Blend_Shapes::resize()
{
	//! By Making `first_pass` static and initializing it to true,
	//! we make sure that the if block will be executed only once.
	/*
	static bool first_pass = true;

	if (first_pass)
	{
	blend_choice_1.setBigSize(getSceneWidth() / 2);
	blend_choice_1.update();
	/*anger_original.setBigSize(getSceneWidth() / 1);
	anger_original.update();
	m_model = face_reference_original;
	first_pass = false;
	}*/
}

void Blend_Shapes::keyEvent(unsigned char key, bool up, int modif)
{
	Scene::keyEvent(key, up, modif);
	key = tolower(key);

	switch (key)
	{
	case 's': m_style_flag ^= FLAG_SHOW_SOLID; break;
	case 'w': m_style_flag ^= FLAG_SHOW_WIRE; break;
	case 'n': m_style_flag ^= FLAG_SHOW_NORMALS; break;
	case 'a': m_style_flag ^= FLAG_SHOW_AXES; break;
	case 'p': m_style_flag ^= FLAG_SHOW_PLANE; break;
	case 'b': m_style_flag ^= FLAG_SHOW_AABB; break;
	case 'l': { // linear blendshaping
		cout << " linear blendshaping " << endl;
		if (differential)
		{
			blend_choice = 0;
			for (int i = 0; i < 10; i++)
			{
				w_d[i] = w[i];
				w[i] = w_l[i];
			}
		}
		linear = true;
		differential = false;
		Change_Expression_Linear();
		break;
	}
	case 'd': { // differential blendshaping
		cout << " differential blenshaping " << endl;
		if (linear)
		{
			blend_choice = 0;
			for (int i = 0; i < 10; i++)
			{
				w_l[i] = w[i];
				w[i] = w_d[i];
			}
		}
		linear = false;
		differential = true;
		Change_Expression_Differential();
		break;
	}
	case 'h': {
		hide = hide ? false : true;
		string message = hide ? (" hide the reference blend shapes ") : (" show the reference blend shapes ");
		cout << message << endl;
		break;
	}
	}
}

void Blend_Shapes::arrowEvent(vvr::ArrowDir dir, int modif)
{
	if (dir == vvr::LEFT)
	{
		{
			if (blend_choice == 0)
				blend_choice = 9;
			else

				blend_choice--;
			cout << "blend shape 1 -> " << face_names[blend_choice] << endl;
		}
	}
	else if (dir == vvr::RIGHT)
	{
		{
			if (blend_choice == 9)
				blend_choice = 0;
			else
				blend_choice++;
			cout << "blend shape 1 -> " << face_names[blend_choice] << endl;
		}
	}
	else if (dir == vvr::UP)
	{
		{
			if (w[blend_choice] > 0.9)
				w[blend_choice] = 0.9;
			else
				w[blend_choice] += 0.1;
		}
		if (linear)
			Change_Expression_Linear();
		else
			Change_Expression_Differential();

	}
	else if (dir == vvr::DOWN)
	{
		{
			if (w[blend_choice] <= 0.1)
				w[blend_choice] = 0.001;
			else
				w[blend_choice] -= 0.1;
		}
		if (linear)
			Change_Expression_Linear();
		else
			Change_Expression_Differential();
	}
}

void Blend_Shapes::draw()
{
	//! Draw plane
	vvr::Colour col;
	if (!hide)
	{
		for (int i = 0; i < 10; i++)
		{
			if (i == blend_choice && linear)
				col = Colour(255, 0, 0);
			else if (i == blend_choice && differential)
				col = Colour(0, 255, 0);
			else
				col = Colour(0, 0, 0);
			if (m_style_flag & FLAG_SHOW_SOLID) blend_1[i].draw(m_obj_col, SOLID);
			if (m_style_flag & FLAG_SHOW_WIRE) blend_1[i].draw(col, WIRE);
			if (m_style_flag & FLAG_SHOW_NORMALS) blend_1[i].draw(col, NORMALS);
		}

	}

	if (m_style_flag & FLAG_SHOW_SOLID) blend_choice_1.draw(m_obj_col, SOLID);
	if (m_style_flag & FLAG_SHOW_WIRE) blend_choice_1.draw(Colour::black, WIRE);
	if (m_style_flag & FLAG_SHOW_NORMALS) blend_choice_1.draw(Colour::black, NORMALS);

}

void Blend_Shapes::change_location(vector<vec> initial_vertices, vector<vec> &new_vertices, float x_axis, float y_axis)
{
	vec norm = { 20 * x_axis,20 * y_axis,0 };

	for (int i = 0; i < initial_vertices.size(); i++)
		new_vertices[i] = initial_vertices[i] + norm;
}


void Blend_Shapes::Change_Expression_Linear()
{
	vector<vec> old_vertices;
	float sum = 0;
	for (int i = 0; i < 10; i++)
		sum += w[i];
	// an den epe3ergastei 3exwrista to 1 kanei synexeia zoom
	old_vertices = original_expressions[0].getVertices();
	for (int j = 0; j<blend_choice_1.getVertices().size(); j++)
		blend_choice_1.getVertices()[j] = (w[0] / sum) * old_vertices[j];
	cout << "blend shape -> " << face_names[0] << " weight " << w[0] / sum << endl;
	for (int i = 1; i < 10; i++)
	{
		cout << "blend shape -> " << face_names[i] << " weight " << w[i] / sum << endl;
		old_vertices = original_expressions[i].getVertices();
		for (int j = 0; j<blend_choice_1.getVertices().size(); j++)
			blend_choice_1.getVertices()[j] += (w[i] / sum) * old_vertices[j];
	}
	blend_choice_1.update();
	cout << " --------------------------- " << endl;
}

void Blend_Shapes::Change_Expression_Differential()
{
	vector<vec> old_vertices;
	vector<vec> face_reference;
	face_reference = original_expressions[0].getVertices();
	float sum = 0;
	for (int i = 0; i < 10; i++)
		sum += w[i];
	// an den epe3ergastei 3exwrista to 1 kanei synexeia zoom
	old_vertices = original_expressions[0].getVertices();
	for (int j = 0; j<blend_choice_1.getVertices().size(); j++)
		blend_choice_1.getVertices()[j] = (w[0] / sum) * (old_vertices[j] - face_reference[j]);
	cout << "blend shape -> " << face_names[0] << " weight " << w[0] / sum << endl;
	for (int i = 1; i < 10; i++)
	{
		cout << "blend shape -> " << face_names[i] << " weight " << w[i] / sum << endl;
		old_vertices = original_expressions[i].getVertices();
		for (int j = 0; j<blend_choice_1.getVertices().size(); j++)
			blend_choice_1.getVertices()[j] += (w[i] / sum) * (old_vertices[j] - face_reference[j]);
	}
	for (int j = 0; j<blend_choice_1.getVertices().size(); j++)
		blend_choice_1.getVertices()[j] += face_reference[j];
	blend_choice_1.update();
	cout << " --------------------------- " << endl;
}

int main(int argc, char* argv[])
{
	try {
		return vvr::mainLoop(argc, argv, new Blend_Shapes);
	}
	catch (std::string exc) {
		cerr << exc << endl;
		return 1;
	}
	catch (...)
	{
		cerr << "Unknown exception" << endl;
		return 1;
	}
}

int Find_Shame(vector<vec> vertices, vec vect)
{
	int found = 0;
	double min_d = 1000, tmp_d;
	for (int i = 0; i < vertices.size(); i++)
	{
		tmp_d = vertices[i].Distance(vect);
		if (tmp_d == 0)
		{
			found = i;
			break;
		}
		else if (tmp_d < min_d)
		{
			min_d = tmp_d;
			found = i;
		}

	}
	return found;
}
