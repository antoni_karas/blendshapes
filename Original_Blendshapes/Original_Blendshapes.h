#include <VVRScene/canvas.h>
#include <VVRScene/mesh.h>
#include <VVRScene/settings.h>
#include <VVRScene/utils.h>
#include <MathGeoLib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <set>
#include <math.h>


#define FLAG_SHOW_AXES       1
#define FLAG_SHOW_WIRE       2
#define FLAG_SHOW_SOLID      4
#define FLAG_SHOW_NORMALS    8
#define FLAG_SHOW_PLANE     16
#define FLAG_SHOW_AABB      32

int m_style_flag;


class Blend_Shapes : public vvr::Scene
{
public:
	Blend_Shapes();
	const char* getName() const { return "Blend Shapes"; }
	void keyEvent(unsigned char key, bool up, int modif) override;
	void Blend_Shapes::change_location(std::vector<vec> initial_vertices, std::vector<vec> &new_vertices, float x_axis, float y_axis);
	void Change_Expression_Linear();
	void Blend_Shapes::Change_Expression_Differential();
	void arrowEvent(vvr::ArrowDir dir, int modif) override;
	
private:
	void draw() override;
	void reset() override;
	void resize() override;
	
private:

	float m_plane_d;
	vvr::Canvas2D m_canvas;
	vvr::Colour m_obj_col;
	vvr::Mesh original_expressions[10], blend_1[10],blend_choice_1;
	vvr::Box3D m_aabb;
	math::vec m_center_mass;
	math::vec m_pca_cen;
	math::vec m_pca_dir;
	math::Plane m_plane;
	std::vector<int> m_intersections;
};

int Find_Shame(std::vector<vec> vertices, vec vect);
