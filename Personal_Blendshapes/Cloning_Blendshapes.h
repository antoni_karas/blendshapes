#include <VVRScene/canvas.h>
#include <VVRScene/mesh.h>
#include <VVRScene/settings.h>
#include <VVRScene/utils.h>
#include <MathGeoLib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <set>
#include <math.h>


#define FLAG_SHOW_AXES       1
#define FLAG_SHOW_WIRE       2
#define FLAG_SHOW_SOLID      4
#define FLAG_SHOW_NORMALS    8
#define FLAG_SHOW_PLANE     16
#define FLAG_SHOW_AABB      32

int m_style_flag;

class Blend_Shapes : public vvr::Scene
{
public:
	Blend_Shapes();
	const char* getName() const { return "Blend Shapes"; }
	void keyEvent(unsigned char key, bool up, int modif) override;
	void Change_Expression_Linear(double wv[10],vvr::Mesh m[10]);
	void Change_Expression_Differential(double wv[10], vvr::Mesh m[10]);
	void arrowEvent(vvr::ArrowDir dir, int modif) override;
	void mousePressed(int x, int y, int modif) override;
	void FindSimillarPoints(std::vector<vec> v_or_exp,std::vector<int> &sim_pts_index);
	std::vector<vvr::Triangle3D> Triangulate(std::vector<vec> c_points);
	void ChangeMesh(std::vector<vec> &vertices, std::vector<vvr::Triangle3D> c_triangles);
	void MakePersonalExpressions();

private:
	void draw() override;
	void reset() override;

private:

	int initial_contr_ind[21] = { 23253, 10561, 9426, 7908, 5682, 5111, 3381, 3088, 11802, 15988, 23676, 24313, 24571, 24721,  24755, 22917,2569, 14395, 4396, 3376, 20318 };
	std::string face_names[10] = { "face-reference", "anger", "cry", "fury", "grin", "laugh", "rage", "sad", "smile", "surprise" };
	bool hide = false, cloning = false;
	bool linear = true, differential = false,original = true, personal = false;
	double v[10] = { 0.1 }, v_l[10] = { 0.1 }, v_d[10] = { 0.1 };
	double w[10] = { 0.1 }, w_l[10] = { 0.1 }, w_d[10] = { 0.1 };
	int blend_choice = 0;
	std::vector<vec> ctrl_points;
	std::vector<int> ctrl_pts_index;
	vvr::Canvas2D m_canvas;
	vvr::Colour m_obj_col;
	vvr::Mesh original_expressions[10], blend_o[10],blend_p[10], personal_expressions[10],blend_expression,scanned_expression;

};

void change_location(std::vector<vec> initial_vertices, std::vector<vec> &new_vertices, float x_axis, float y_axis);
int FindTriangle3D(std::vector<vvr::Triangle3D> Triangles, vvr::Point2D p);
int FindNearestVertice(std::vector<vec> vecs, vvr::Point2D p);
math::vec BarycentricInterpolation(vvr::Triangle3D tr, vvr::Point2D p);
std::vector<vec> FindDiference(std::vector<vec> face_reference, std::vector<vec> face_expression, std::vector<vec> c_points, std::vector<int> sim_pts_inds);
void Help();