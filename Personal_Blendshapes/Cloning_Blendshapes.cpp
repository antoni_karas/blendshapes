#include "Cloning_Blendshapes.h"

using namespace std;
using namespace vvr;

Blend_Shapes::Blend_Shapes()
{
	//! Load settings.
	vvr::Shape::DEF_LINE_WIDTH = 4;
	vvr::Shape::DEF_POINT_SIZE = 10;
	m_perspective_proj = true;
	m_bg_col = Colour("768E77");
	m_obj_col = Colour("454545");
	const string objDir = getBasePath() + "face-poses/";
	cout << " please wait to load the blend shapes " << endl;
	// -----------> load original expressions <--------------
	string objFile ;
	objFile = objDir + "face-reference.obj";
	original_expressions[0] = vvr::Mesh(objFile);
	personal_expressions[0] = original_expressions[0];
	blend_o[0] = personal_expressions[0];
	blend_p[0] = personal_expressions[0];
	objFile = objDir + "face-01-anger.obj";
	original_expressions[1] = vvr::Mesh(objFile);
	personal_expressions[1] = original_expressions[1];
	blend_o[1] = personal_expressions[1];
	blend_p[1] = personal_expressions[1];
	objFile = objDir + "face-02-cry.obj";
	original_expressions[2] = vvr::Mesh(objFile);
	personal_expressions[2] = original_expressions[2];
	blend_o[2] = personal_expressions[2];
	blend_p[2] = personal_expressions[2];
	objFile = objDir + "face-03-fury.obj";
	original_expressions[3] = vvr::Mesh(objFile);
	personal_expressions[3] = original_expressions[3];
	blend_o[3] = personal_expressions[3];
	blend_p[3] = personal_expressions[3];
	objFile = objDir + "face-04-grin.obj";
	original_expressions[4] = vvr::Mesh(objFile);
	personal_expressions[4] = original_expressions[4];
	blend_o[4] = personal_expressions[4];
	blend_p[4] = personal_expressions[4];
	objFile = objDir + "face-05-laugh.obj";
	original_expressions[5] = vvr::Mesh(objFile);
	personal_expressions[5] = original_expressions[5];
	blend_o[5] = personal_expressions[5];
	blend_p[5] = personal_expressions[5];
	objFile = objDir + "face-06-rage.obj";
	original_expressions[6] = vvr::Mesh(objFile);
	personal_expressions[6] = original_expressions[6];
	blend_o[6] = personal_expressions[6];
	blend_p[6] = personal_expressions[6];
	objFile = objDir + "face-07-sad.obj";
	original_expressions[7] = vvr::Mesh(objFile);
	personal_expressions[7] = original_expressions[7];
	blend_o[7] = personal_expressions[7];
	blend_p[7] = personal_expressions[7];
	objFile = objDir + "face-08-smile.obj";
	original_expressions[8] = vvr::Mesh(objFile);
	personal_expressions[8] = original_expressions[8];
	blend_o[8] = personal_expressions[8];
	blend_p[8] = personal_expressions[8];
	objFile = objDir + "face-09-surprise.obj";
	original_expressions[9] = vvr::Mesh(objFile);
	personal_expressions[9] = original_expressions[9];
	blend_o[9] = personal_expressions[9];
	blend_p[9] = personal_expressions[9];
	
	// load scanned face reference
	objFile = objDir + "antoni_new.obj";
	scanned_expression = vvr::Mesh(objFile);
	scanned_expression.setBigSize(original_expressions[0].getMaxSize());
	scanned_expression.setTransform(original_expressions[0].getTransform());
	scanned_expression.update();
	//
	blend_expression = personal_expressions[0];
	// -------------------------------------------------------------
	cout << " loading the blend shapes has almost finished " << endl;
	change_location(original_expressions[0].getVertices(), blend_o[0].getVertices(), 0.0, 2.5);
	change_location(original_expressions[1].getVertices(), blend_o[1].getVertices(), 1.25, 2.02);
	change_location(original_expressions[2].getVertices(), blend_o[2].getVertices(), 2.38, 0.77);
	change_location(original_expressions[3].getVertices(), blend_o[3].getVertices(), 2.38, -0.77);
	change_location(original_expressions[4].getVertices(), blend_o[4].getVertices(), 1.25, -2.02);
	change_location(original_expressions[5].getVertices(), blend_o[5].getVertices(), 0.0, -2.5);
	change_location(original_expressions[6].getVertices(), blend_o[6].getVertices(), -1.25, -2.02);
	change_location(original_expressions[7].getVertices(), blend_o[7].getVertices(), -2.38, -0.77);
	change_location(original_expressions[8].getVertices(), blend_o[8].getVertices(), -2.38, 0.77);
	change_location(original_expressions[9].getVertices(), blend_o[9].getVertices(), -1.25, 2.02);
	cout << " loading the blendshapes is finished " << endl;
	for (int i = 0; i < 21; i++)
	{
		ctrl_points.push_back(scanned_expression.getVertices()[initial_contr_ind[i]]);
		ctrl_pts_index.push_back(initial_contr_ind[i]);
	}
	MakePersonalExpressions();
	reset();

}

void Blend_Shapes::reset()
{

	Scene::reset();
	//! Define what will be vissible by default
	m_style_flag = 0;
	m_style_flag |= FLAG_SHOW_SOLID;
	m_style_flag |= FLAG_SHOW_WIRE;
	m_style_flag |= FLAG_SHOW_AXES;
	m_style_flag |= FLAG_SHOW_AABB;
	Help();
}

void Blend_Shapes::keyEvent(unsigned char key, bool up, int modif)
{
	Scene::keyEvent(key, up, modif);
	key = tolower(key);
	
	switch (key)
	{
	case 's': m_style_flag ^= FLAG_SHOW_SOLID; break;
	case 'w': m_style_flag ^= FLAG_SHOW_WIRE; break;
	case 'n': m_style_flag ^= FLAG_SHOW_NORMALS; break;
	case '?' :
	{
		Help();
		break;
	}
	case 'h':
	{
		hide = hide ? false : true;
		string message = hide ? (" hide the reference blend shapes ") : (" show the reference blend shapes ");
		cout << message << endl;
		break;
	}
	case 'o' :
	{
		cloning = false;
		original = original ? false : true;
		personal = original ? false : true;
		if (original)
		{
			// it was personal and it became original
			if (linear)
			{
				// personal linear -> original linear
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					v_l[i] = v[i];		// store working weights to personal linear weights 
					w[i] = w_l[i];		// change working weights to original linear weights
				}
				Change_Expression_Linear(w, original_expressions);
			}
			if (differential)
			{
				// personal differential -> original differential
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					v_d[i] = v[i];		// store working weights to personal differential weights 
					w[i] = w_d[i];		// change working weights to original differential weights
				}
				Change_Expression_Differential(w, original_expressions);
			}
		}
		if (personal)
		{
			// it was original and it became personal
			if (linear)
			{
				// original linear -> personal linear
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					w_l[i] = w[i];		// store working weights to original linear weights 
					v[i] = v_l[i];		// change working weights to personal linear weights
				}
				Change_Expression_Linear(v,personal_expressions);
			}
			if (differential)
			{
				// original differential -> personal differential
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					w_d[i] = w[i];		// store working weights to original differential weights 
					v[i] = v_d[i];		// change working weights to personal differential weights
				}
				Change_Expression_Differential(v, personal_expressions);
			}
		}
		break;
	}
	case 'c' :
	{
		cout << " start cloning " << endl;
		personal = false;
		original = false;
		break;
	}
	case 'p':
	{
		cloning = false;
		personal = personal ? false : true;
		original = personal ? false : true;
		if (original)
		{
			// it was personal and it became original
			if (linear)
			{
				// personal linear -> original linear
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					v_l[i] = v[i];		// store working weights to personal linear weights 
					w[i] = w_l[i];		// change working weights to original linear weights
				}
			}
			if (differential)
			{
				// personal differential -> original differential
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					v_d[i] = v[i];		// store working weights to personal differential weights 
					w[i] = w_d[i];		// change working weights to original differential weights
				}
			}
		}
		if (personal)
		{
			// it was original and it became personal
			if (linear)
			{
				// original linear -> personal linear
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					w_l[i] = w[i];		// store working weights to original linear weights 
					v[i] = v_l[i];		// change working weights to personal linear weights
				}
			}
			if (differential)
			{
				// original differential -> personal differential
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					w_d[i] = w[i];		// store working weights to original differential weights 
					v[i] = v_d[i];		// change working weights to personal differential weights
				}
			}
		}
		break;
	} 
	case 'l': { // linear blendshaping <----------------------------------------------------- continue editing
		cout << " linear blendshaping " << endl;
		linear = linear ? false : true;
		differential = linear ? false : true;
		if (original)
		{
			if (differential)
			{
				// original linear -> original differential
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					w_l[i] = w[i];
					w[i] = w_d[i];
				}
				Change_Expression_Differential(w,original_expressions);
			}
			if (linear)
			{
				// original differential -> original linear
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					w_d[i] = w[i];
					w[i] = w_l[i];
				}
				Change_Expression_Differential(w, original_expressions);
			}
		}
		if (personal)
		{
			if (differential)
			{
				// personal linear -> personal differential
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					v_l[i] = v[i];
					v[i] = v_d[i];
				}
				Change_Expression_Differential(v, personal_expressions);
			}
			if (linear)
			{
				// original differential -> original linear
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					v_d[i] = v[i];
					v[i] = v_l[i];
				}
				Change_Expression_Differential(v, personal_expressions);
			}
		}

		break;
	}
	case 'd': { 
		// differential blendshaping
		differential = differential ? false : true;
		linear = differential ? false : true;
		
		if (original)
		{
			if (differential)
			{
				// original linear -> original differential
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					w_l[i] = w[i];
					w[i] = w_d[i];
				}
				Change_Expression_Differential(w, original_expressions);
			}
			if (linear)
			{
				// original differential -> original linear
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					w_d[i] = w[i];
					w[i] = w_l[i];
				}
				Change_Expression_Differential(w, original_expressions);
			}
		}
		if (personal)
		{
			if (differential)
			{
				// personal linear -> personal differential
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					v_l[i] = v[i];
					v[i] = v_d[i];
				}
				Change_Expression_Differential(v, personal_expressions);
			}
			if (linear)
			{
				// original differential -> original linear
				blend_choice = 0;
				for (int i = 0; i < 10; i++)
				{
					v_d[i] = v[i];
					v[i] = v_l[i];
				}
				Change_Expression_Differential(v, personal_expressions);
			}
		}

		break;
	}
	}
}

void Blend_Shapes::arrowEvent(vvr::ArrowDir dir, int modif)
{
	if (dir == vvr::LEFT)
	{
		{
			if (blend_choice == 0)
				blend_choice = 9;
			else

				blend_choice--;
			cout << "blend shape 1 -> " << face_names[blend_choice] << endl;
		}
	}
	else if (dir == vvr::RIGHT)
	{
		{
			if (blend_choice == 9)
				blend_choice = 0;
			else
				blend_choice++;
			cout << "blend shape 1 -> " << face_names[blend_choice] << endl;
		}
	}
	else if (dir == vvr::UP)
	{
		if (original)
		{
			if (w[blend_choice] > 0.9)
				w[blend_choice] = 0.9;
			else
				w[blend_choice] += 0.1;
			if (linear)
				Change_Expression_Linear(w,original_expressions);
			if (differential)
				Change_Expression_Differential(w,original_expressions);
		}
		if (personal)
		{
			if (v[blend_choice] > 0.9)
				v[blend_choice] = 0.9;
			else
				v[blend_choice] += 0.1;
			if (linear)
				Change_Expression_Linear(v, personal_expressions);
			if (differential)
				Change_Expression_Differential(v, personal_expressions);
		}
		
	}
	else if (dir == vvr::DOWN)
	{
		if (original)
		{
			if (w[blend_choice] <= 0.1)
				w[blend_choice] = 0.001;
			else
				w[blend_choice] -= 0.1;
			if (linear)
				Change_Expression_Linear(w,original_expressions);
			if (differential)
				Change_Expression_Differential(w,original_expressions);
		}
		if (personal)
		{
			if (v[blend_choice] <= 0.1)
				v[blend_choice] = 0.001;
			else
				v[blend_choice] -= 0.1;
			if (linear)
				Change_Expression_Linear(v, personal_expressions);
			if (differential)
				Change_Expression_Differential(v, personal_expressions);
		}
		
	}
}

void Blend_Shapes::mousePressed(int x, int y, int modif)
{

	cout << "Mouse pressed - [" << (float)x  << ", " << (float)y  << "]" << endl;
	
}

void Blend_Shapes::draw()
{
	vvr::Colour col;
	
	if (!hide)
	{
		for (int i = 0; i < 10; i++)
		{
			if (i == blend_choice && linear)
				col = Colour(255, 0, 0);
			else if (i == blend_choice && differential)
				col = Colour(0, 255, 0);
			else
				col = Colour(0, 0, 0);
			if (original)
			{
				if (m_style_flag & FLAG_SHOW_SOLID) blend_o[i].draw(m_obj_col, SOLID);
				if (m_style_flag & FLAG_SHOW_WIRE) blend_o[i].draw(col, WIRE);
				if (m_style_flag & FLAG_SHOW_NORMALS) blend_o[i].draw(col, NORMALS);
			}
			if (personal)
			{
				if (m_style_flag & FLAG_SHOW_SOLID) blend_p[i].draw(m_obj_col, SOLID);
				if (m_style_flag & FLAG_SHOW_WIRE) blend_p[i].draw(col, WIRE);
				if (m_style_flag & FLAG_SHOW_NORMALS) blend_p[i].draw(col, NORMALS);
			}

		}
	}
		
		if (m_style_flag & FLAG_SHOW_SOLID) blend_expression.draw(m_obj_col, SOLID);
		if (m_style_flag & FLAG_SHOW_WIRE) blend_expression.draw(Colour::black, WIRE);
		if (m_style_flag & FLAG_SHOW_NORMALS) blend_expression.draw(Colour::black, NORMALS);
}

void Blend_Shapes::Change_Expression_Linear(double wv[10],Mesh m[10])
{
	vector<vec> verts[10];
	for (int i = 0; i < 10; i++)
		verts[i] = m[i].getVertices();
	vector<vec> old_vertices;
	float sum = 0;
	for (int i = 0; i < 10; i++)
		sum += wv[i];
	// an den epe3ergastei 3exwrista to 1 kanei synexeia zoom
	old_vertices = verts[0];
	for (int j = 0; j < blend_expression.getVertices().size(); j++)
		blend_expression.getVertices()[j] = (wv[0] / sum) * old_vertices[j];
	cout << "blend shape -> " << face_names[0] << " weight " << wv[0] / sum << endl;
	for (int i = 1; i < 10; i++)
	{
		cout << "blend shape -> " << face_names[i] << " weight " << wv[i] / sum << endl;
		old_vertices = verts[i];
		for (int j = 0; j < blend_expression.getVertices().size(); j++)
			blend_expression.getVertices()[j] += (wv[i] / sum) * old_vertices[j];
	}
	blend_expression.update();
	cout << " --------------------------- " << endl;
}

void Blend_Shapes::Change_Expression_Differential(double wv[10], Mesh m[10])
{
	vector<vec> verts[10];
	for (int i = 0; i < 10; i++)
		verts[i] = m[i].getVertices();
	vector<vec> old_vertices;
	vector<vec> face_reference;
	face_reference = verts[0];
	float sum = 0;
	for (int i = 0; i < 10; i++)
		sum += wv[i];
	// an den epe3ergastei 3exwrista to 1 kanei synexeia zoom
	old_vertices = verts[0];
	for (int j = 0; j < blend_expression.getVertices().size(); j++)
		blend_expression.getVertices()[j] = (wv[0] / sum) * (old_vertices[j] - face_reference[j]);
	cout << "blend shape -> " << face_names[0] << " weight " << wv[0] / sum << endl;
	for (int i = 1; i < 10; i++)
	{
		cout << "blend shape -> " << face_names[i] << " weight " << wv[i] / sum << endl;
		old_vertices = verts[i];
		for (int j = 0; j < blend_expression.getVertices().size(); j++)
			blend_expression.getVertices()[j] += (wv[i] / sum) * (old_vertices[j] - face_reference[j]);
	}
	for (int j = 0; j < blend_expression.getVertices().size(); j++)
		blend_expression.getVertices()[j] += face_reference[j];
	blend_expression.update();
	cout << " --------------------------- " << endl;
}

void Blend_Shapes::FindSimillarPoints(vector<vec> v_or_exp,vector<int> &sim_pts_index)
{

	for (int i = 0; i < ctrl_pts_index.size(); i++)
	{
		vec v = ctrl_points[i];
		int min_ind = 0;
		float min_dist = 1000,d;
		for (int j = 0; j < v_or_exp.size(); j++)
		{
			d = (v - v_or_exp[j]).Length();
			if (d < min_dist)
			{
				min_ind = j;
				min_dist = d;
			}
		}
		personal_expressions[0].getVertices()[min_ind] = v;
		sim_pts_index.push_back(min_ind);
	}
}

vector<Triangle3D> Blend_Shapes::Triangulate(vector<vec> c_points)
{
	vector<Triangle3D> c_triangles;
	vec v3, v1, v2;
	v1 = c_points[16];
	for (int i = 0; i < 15; i++)
	{
		v2 = c_points[i];
		v3 = c_points[i + 1];
		c_triangles.push_back(Triangle3D(v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z,Colour::orange));
	}
	v2 = c_points[0];
	c_triangles.push_back(Triangle3D(v1.x, v1.y, v1.z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, Colour::orange));
	int index;
	for (int i = 17; i < c_points.size(); i++)
	{
		index = FindTriangle3D(c_triangles, Point2D((float)c_points[i].x, (float)c_points[i].y));
		v1 = { (float)c_triangles[index].x1,(float)c_triangles[index].y1,(float)c_triangles[index].z1 };
		v2 = { (float)c_triangles[index].x2,(float)c_triangles[index].y2,(float)c_triangles[index].z2 };
		v3 = { (float)c_triangles[index].x3,(float)c_triangles[index].y3,(float)c_triangles[index].z3 };
		c_triangles.erase(c_triangles.begin() + index);
		c_triangles.push_back(Triangle3D(c_points[i].x, c_points[i].y, c_points[i].z, v2.x, v2.y, v2.z, v3.x, v3.y, v3.z, Colour::green));
		c_triangles.push_back(Triangle3D(c_points[i].x, c_points[i].y, c_points[i].z, v1.x, v1.y, v1.z, v3.x, v3.y, v3.z, Colour::darkGreen));
		c_triangles.push_back(Triangle3D(c_points[i].x, c_points[i].y, c_points[i].z, v2.x, v2.y, v2.z, v1.x, v1.y, v1.z, Colour::yellowGreen));
	}
	return c_triangles;
}

void Blend_Shapes::ChangeMesh(vector<vec> &vertices,vector<Triangle3D> c_triangles)
{
	int index;
	double max_z = 0, max_x = 0, max_y = 0;
	for (int i = 0; i < vertices.size(); i++)
	{
		if (vertices[i].x > max_x)
			max_x = vertices[i].x;
		if (vertices[i].y > max_y)
			max_y = vertices[i].y;
		if (vertices[i].z > max_z)
			max_z = vertices[i].z;
	}
	vec tmp_vert;
	int wrong_vert = 0;
	for (int i = 0; i < vertices.size(); i++)
	{
		index = FindTriangle3D(c_triangles, Point2D((float)vertices[i].x, (float)vertices[i].y));
		tmp_vert = BarycentricInterpolation(c_triangles[index], Point2D((float)vertices[i].x, (float)vertices[i].y));
		if ((tmp_vert.x / (tmp_vert.x + max_x) < 0.55 ) && (tmp_vert.y / (tmp_vert.y + max_y) < 0.55) && (tmp_vert.z / (tmp_vert.z + max_z) < 0.55))
			vertices[i] = tmp_vert;
		else
			wrong_vert++;
	}
	cout << " didn't processed corectly " << ((float)wrong_vert/(float)vertices.size()) * 100.0 << " % " << endl;
}

void Blend_Shapes::MakePersonalExpressions()
{
	cout << " expression " << face_names[0] << endl;
	vector<int> sim_pts_index;
	FindSimillarPoints(personal_expressions[0].getVertices(),sim_pts_index);
	vector<vvr::Triangle3D> c_triangles;
	c_triangles = Triangulate(ctrl_points);
	ChangeMesh(personal_expressions[0].getVertices(),c_triangles);
	vector<vec> c_points;
	for (int i = 1; i < 10; i++)
	{
		cout << " expression " << face_names[i] << endl;
		c_points = FindDiference(original_expressions[0].getVertices(), original_expressions[i].getVertices(), ctrl_points, sim_pts_index);
		c_triangles = Triangulate(c_points);
		ChangeMesh(personal_expressions[i].getVertices(), c_triangles);
		c_points.clear();
		c_triangles.clear();
	}

	change_location(personal_expressions[0].getVertices(), blend_p[0].getVertices(), 0.0, 2.5);
	change_location(personal_expressions[1].getVertices(), blend_p[1].getVertices(), 1.25, 2.02);
	change_location(personal_expressions[2].getVertices(), blend_p[2].getVertices(), 2.38, 0.77);
	change_location(personal_expressions[3].getVertices(), blend_p[3].getVertices(), 2.38, -0.77);
	change_location(personal_expressions[4].getVertices(), blend_p[4].getVertices(), 1.25, -2.02);
	change_location(personal_expressions[5].getVertices(), blend_p[5].getVertices(), 0.0, -2.5);
	change_location(personal_expressions[6].getVertices(), blend_p[6].getVertices(), -1.25, -2.02);
	change_location(personal_expressions[7].getVertices(), blend_p[7].getVertices(), -2.38, -0.77);
	change_location(personal_expressions[8].getVertices(), blend_p[8].getVertices(), -2.38, 0.77);
	change_location(personal_expressions[9].getVertices(), blend_p[9].getVertices(), -1.25, 2.02);
	
}

int main(int argc, char* argv[])
{
	try {
		return vvr::mainLoop(argc, argv, new Blend_Shapes);
	}
	catch (std::string exc) {
		cerr << exc << endl;
		return 1;
	}
	catch (...)
	{
		cerr << "Unknown exception" << endl;
		return 1;
	}

}

void change_location(vector<vec> initial_vertices, vector<vec> &new_vertices, float x_axis, float y_axis)
{
	vec norm = { 20 * x_axis,20 * y_axis,0 };

	for (int i = 0; i < initial_vertices.size(); i++)
		new_vertices[i] = initial_vertices[i] + norm;
}

int FindTriangle3D(vector<vvr::Triangle3D> Triangles, Point2D p)
{
	int tr = -1, i = 0;
	vec A, B, C, a;
	a = {float(p.x), float(p.y), 0};
	float area_in, area_1, area_2, area_3,min_area=1000 ;
	for (int i=0;i<Triangles.size();i++)
	{
		A = { (float)Triangles[i].x1,(float)Triangles[i].y1,0 };
		B = { (float)Triangles[i].x2,(float)Triangles[i].y2,0 };
		C = { (float)Triangles[i].x3,(float)Triangles[i].y3,0 };
		area_in = 0.5*((A - B).Cross(A - C)).Length();
		area_1 = 0.5*((A - a).Cross(A - C)).Length();
		area_2 = 0.5*((A - a).Cross(A - B)).Length();
		area_3 = 0.5*((B - a).Cross(B - C)).Length();
		if (abs(area_in - area_1 - area_2 - area_3) < min_area)
		{
			tr = i;
			min_area = abs(area_in - area_1 - area_2 - area_3);
		}
	}

	return tr;
}

vec BarycentricInterpolation(vvr::Triangle3D tr, vvr::Point2D p)
{
	vec p3d;
	vec A, B, C, a;
	float area_in, area_1, area_2, area_3;
	A = { (float)tr.x1, (float)tr.y1, 0 };
	B = { (float)tr.x2, (float)tr.y2, 0 };
	C = { (float)tr.x3, (float)tr.y3, 0 };
	a = { (float)p.x,(float)p.y ,0 };
	area_in = 0.5*((A - B).Cross(A - C)).Length();
	area_1 = 0.5*((A - a).Cross(A - C)).Length();
	area_2 = 0.5*((A - a).Cross(A - B)).Length();
	area_3 = 0.5*((B - a).Cross(B - C)).Length();
	A = { (float)tr.x1, (float)tr.y1, (float)tr.z1 };
	B = { (float)tr.x2, (float)tr.y2, (float)tr.z2 };
	C = { (float)tr.x3, (float)tr.y3, (float)tr.z3 };
	a = A*(area_3 / area_in) + B*(area_1 / area_in) + C*(area_2 / area_in);

	return{ a.x, a.y, a.z };
}

int FindNearestVertice(vector<vec> verts, Point2D p)
{
	int f = 0;
	float min_dist = 1000;
	vec a, b;
	a = { float(p.x), float(p.y), 0 };
	for (int i = 0; i < verts.size(); i++)
	{
		b = { verts[i].x,verts[i].y,0 };
		if ((a - b).Length() < min_dist)
		{
			f = i;
			min_dist = (a - b).Length();
		}
	}
	return f;
}

vector<vec> FindDiference(vector<vec> face_reference, vector<vec> face_expression, vector<vec> c_points, vector<int> sim_pts_inds)
{
	vec dif;
	vector<vec> n_c_points;
	int index;
	for (int i = 0; i < sim_pts_inds.size(); i++)
	{
		index = sim_pts_inds[i];
		dif = face_expression[index] - face_reference[index];
		n_c_points.push_back(dif + c_points[i]);
	}

	return n_c_points;
}

void Help()
{
	cout << " ----------**********----------" << endl;
	cout << " ? -> keys " << endl;
	cout << " a -> add control points " << endl;
	cout << " c -> compare personal and original expressions " << endl;
	cout << " d -> differential blendshaping " << endl;
	cout << " h -> show-hide blendshapes " << endl;
	cout << " l -> linear blendshaping " << endl;
	cout << " p -> show-hide control points " << endl;
	cout << " t -> show-hide triangles used for barycentric interpolation " << endl;
	cout << " ----------**********----------" << endl;
}